#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_m10lte.mk

COMMON_LUNCH_CHOICES := \
    lineage_m10lte-user \
    lineage_m10lte-userdebug \
    lineage_m10lte-eng
