# Unpinned blobs from m10ltedd-user 10 QP1A.190711.020 M105FDDS4CVG1 release-keys

# Audio
vendor/bin/hw/android.hardware.audio@2.0-service
vendor/etc/init/android.hardware.audio@2.0-service.rc
vendor/lib/hw/android.hardware.audio.effect@2.0-impl.so
vendor/lib/hw/android.hardware.audio@2.0-impl.so
vendor/lib/hw/audio.primary.default.so
vendor/lib/hw/audio.primary.exynos7870.so
vendor/lib/hw/audio.r_submix.default.so
vendor/lib/hw/audio.usb.default.so
vendor/lib/soundfx/libLifevibes_lvverx.so
vendor/lib/soundfx/libLifevibes_lvvetx.so
vendor/lib/libLifevibes_lvverx.so
vendor/lib/libLifevibes_lvvetx.so
vendor/lib/libSamsungPostProcessConvertor.so
vendor/lib/lib_SamsungRec_06006.so
vendor/lib/lib_SoundAlive_SRC384_ver320.so
vendor/lib/lib_soundaliveresampler.so
vendor/lib/libalsautils.so
vendor/lib/libaudio-ril.so
vendor/lib/libfloatingfeature.so
vendor/lib/libpreprocessing_nxp.so
vendor/lib/librecordalive.so
vendor/lib/libsamsungDiamondVoice.so
vendor/lib/libsecaudioinfo.so
vendor/lib/libsecnativefeature.so
vendor/lib/libtfa98xx.so
vendor/lib/libvndsecril-client.so
vendor/lib/vendor.samsung.hardware.audio@1.0.so

# Audio (FX modules)
vendor/lib/soundfx/libaudioeffectoffload.so
vendor/lib/soundfx/libaudiosaplus_sec.so
vendor/lib/soundfx/libdynproc.so
vendor/lib/soundfx/libgearvr.so
vendor/lib/soundfx/libmysound.so
vendor/lib/soundfx/libmyspace.so
vendor/lib/soundfx/libplaybackrecorder.so
vendor/lib/soundfx/libsamsungSoundbooster_plus.so
vendor/lib/soundfx/libswdap.so
vendor/lib/libHMT.so

# Audio configs
etc/audio_effects.conf
vendor/etc/a2dp_audio_policy_configuration.xml
vendor/etc/audio_effects.xml
vendor/etc/audio_effects_common.conf
vendor/etc/audio_effects_sec.xml
vendor/etc/audio_policy_configuration.xml
vendor/etc/audio_policy_configuration_sec.xml
vendor/etc/audio_policy_volumes.xml
vendor/etc/audio_policy_volumes_sec.xml
vendor/etc/default_volume_tables.xml
vendor/etc/hearing_aid_audio_policy_configuration.xml
vendor/etc/mixer_paths.xml
vendor/etc/playback_record_audio_policy_configuration.xml
vendor/etc/r_submix_audio_policy_configuration.xml
vendor/etc/tms_audio_policy_configuration.xml
vendor/etc/usb_audio_policy_configuration.xml

# Bluetooth
vendor/bin/hw/android.hardware.bluetooth@1.0-service
vendor/etc/init/android.hardware.bluetooth@1.0-service.rc
vendor/lib/hw/android.hardware.bluetooth@1.0-impl.so
vendor/lib/vendor.samsung.hardware.bluetooth@2.0.so

# Camera
vendor/lib/hw/camera.exynos7870.so
vendor/lib/camera.device@1.0-impl.so
vendor/lib/camera.device@3.2-impl.so
vendor/lib/camera.device@3.3-impl.so
vendor/lib/camera.device@3.4-impl.so
vendor/lib/camera.device@3.5-impl.so
vendor/lib/libGrallocWrapper.so
vendor/lib/libacryl.so
vendor/lib/libcsc.so
vendor/lib/libexynoscamera.so
vendor/lib/libexynoscamera3.so
vendor/lib/libexynosgscaler.so
vendor/lib/libexynosscaler.so
vendor/lib/libexynosutils.so
vendor/lib/libexynosv4l2.so
vendor/lib/libgiantmscl.so
vendor/lib/libhwjpeg.so
vendor/lib/libion_exynos.so
vendor/lib/libsensorlistener.so
vendor/lib/libsensorndkbridge.so
vendor/lib/libstainkiller.so
vendor/lib/libuniplugin.so

# CAS
vendor/bin/hw/android.hardware.cas@1.1-service
vendor/etc/init/android.hardware.cas@1.1-service.rc
vendor/etc/vintf/manifest/android.hardware.cas@1.1-service.xml

# Configstore
vendor/bin/hw/android.hardware.configstore@1.1-service
vendor/etc/init/android.hardware.configstore@1.1-service.rc
vendor/lib/libhwminijail.so

# Display
apex/com.android.media.swcodec/lib/android.hardware.graphics.common@1.0.so
apex/com.android.media.swcodec/lib/android.hardware.graphics.common@1.1.so
apex/com.android.media.swcodec/lib/android.hardware.graphics.common@1.2.so
lib/vndk-sp-29/android.hardware.graphics.common@1.0.so
lib/vndk-sp-29/android.hardware.graphics.common@1.1.so
lib/vndk-sp-29/android.hardware.graphics.common@1.2.so
lib/vndk-sp-29/android.hardware.graphics.mapper@2.0.so
lib/vndk-sp-29/android.hardware.graphics.mapper@2.1.so
lib/vndk-sp-29/android.hardware.graphics.mapper@3.0.so
vendor/bin/hw/android.hardware.graphics.allocator@2.0-service
vendor/bin/hw/android.hardware.graphics.composer@2.1-service
vendor/bin/hw/android.hardware.memtrack@1.0-service
vendor/etc/init/android.hardware.graphics.allocator@2.0-service.rc
vendor/etc/init/android.hardware.graphics.composer@2.1-service.rc
vendor/etc/init/android.hardware.memtrack@1.0-service.rc
vendor/lib/egl/libGLES_mali.so
vendor/lib/hw/android.hardware.graphics.allocator@2.0-impl.so
vendor/lib/hw/android.hardware.graphics.composer@2.1-impl.so
vendor/lib/hw/android.hardware.graphics.mapper@2.0-impl.so
vendor/lib/hw/android.hardware.memtrack@1.0-impl.so
vendor/lib/hw/gralloc.default.so
vendor/lib/hw/gralloc.exynos7870.so
vendor/lib/hw/hwcomposer.exynos7870.so
vendor/lib/hw/memtrack.exynos7870.so
vendor/lib/hw/vulkan.universal7870.so
vendor/lib/libExynosHWCService.so
vendor/lib/libexynosdisplay.so
vendor/lib/libhdmi.so
vendor/lib/libhwc2onfbadapter.so
vendor/lib/libhwcutils.so
vendor/lib/libmpp.so
vendor/lib/libvirtualdisplay.so

# Dolby
lib/libdeccfg.so
lib/libprofileparamstorage.so
vendor/etc/dolby/dax-default.xml

# DRM
vendor/bin/hw/android.hardware.drm@1.0-service
vendor/bin/hw/android.hardware.drm@1.2-service.clearkey
vendor/bin/hw/android.hardware.drm@1.2-service.widevine
vendor/etc/init/android.hardware.drm@1.0-service.rc
vendor/etc/init/android.hardware.drm@1.2-service.clearkey.rc
vendor/etc/init/android.hardware.drm@1.2-service.widevine.rc
vendor/lib/hw/android.hardware.drm@1.0-impl.so
vendor/lib/mediacas/libclearkeycasplugin.so
vendor/lib/mediadrm/libdrmclearkeyplugin.so
vendor/lib/mediadrm/libwvdrmengine.so
vendor/lib/libMcClient.so
vendor/lib/liboemcrypto.so
vendor/lib/libwvhidl.so

# Gatekeeper
vendor/bin/hw/android.hardware.gatekeeper@1.0-service
vendor/etc/init/android.hardware.gatekeeper@1.0-service.rc
vendor/lib/hw/gatekeeper.exynos7870.so
vendor/lib/android.hardware.gatekeeper@1.0-impl.so

# GNSS
vendor/lib/hw/android.hardware.gnss@2.0-impl.so

# Health
vendor/bin/hw/android.hardware.health@2.0-service.samsung
vendor/etc/init/android.hardware.health@2.0-service.samsung.rc
vendor/lib/vendor.samsung.hardware.health@1.0.so

# Keymaster
vendor/bin/hw/android.hardware.keymaster@3.0-service
vendor/etc/init/android.hardware.keymaster@3.0-service.rc
vendor/lib/hw/android.hardware.keymaster@3.0-impl.so
vendor/lib/hw/keystore.mdfpp.so
vendor/lib/libkeymaster2_mdfpp.so
vendor/lib/libkeymaster3device.so
vendor/lib/libkeymaster_helper_vendor.so
vendor/lib/libskeymaster3device.so

# Light
vendor/lib/hw/lights.universal7870.so

# Local time
vendor/lib/hw/local_time.default.so

# Media (OMX)
etc/seccomp_policy/mediacodec.policy
lib/libhdcp2.so
lib/libsavscmn.so
lib/libsecnativefeature.so
lib/libsfextcp.so
lib/libstagefright_codecbase.so
lib/libstagefright_hdcp.so
lib/libstagefright_httplive_sec.so
lib/libstagefright_local_cache.so
lib/libstagefright_softomx_plugin.so
vendor/bin/hw/android.hardware.media.omx@1.0-service
vendor/etc/init/android.hardware.media.omx@1.0-service.rc
vendor/etc/seccomp_policy/mediacodec.policy
vendor/lib/libstagefright_bufferqueue_helper_vendor.so
vendor/lib/libstagefright_omx_vendor.so
vendor/lib/libstagefright_soft_ac4dec.so
vendor/lib/libstagefright_soft_ddpdec.so
vendor/lib/libstagefrighthw.so

# Media configs
vendor/etc/media_codecs.xml
vendor/etc/media_codecs_ac4.xml
vendor/etc/media_codecs_ddp.xml
vendor/etc/media_codecs_dolby_audio.xml
vendor/etc/media_codecs_google_audio.xml
vendor/etc/media_codecs_google_telephony.xml
vendor/etc/media_codecs_google_video.xml
vendor/etc/media_codecs_performance.xml
vendor/etc/media_codecs_performance_c2.xml
vendor/etc/media_codecs_sec_ape.xml
vendor/etc/media_codecs_sec_primary.xml
vendor/etc/media_codecs_sec_qcp.xml
vendor/etc/media_codecs_sec_secondary.xml
vendor/etc/media_codecs_sec_video_primary.xml
vendor/etc/media_profiles_V1_0.xml

# Power
vendor/bin/hw/android.hardware.power@1.0-service
vendor/etc/init/android.hardware.power@1.0-service.rc
vendor/lib/hw/android.hardware.power@1.0-impl.so
vendor/lib/hw/power.default.so
vendor/lib/hw/power.universal7870.so

# RenderScript
lib/vndk-sp-29/android.hardware.renderscript@1.0.so

# Sensors
vendor/bin/hw/android.hardware.sensors@1.0-service
vendor/etc/init/android.hardware.sensors@1.0-service.rc
vendor/lib/hw/android.hardware.sensors@1.0-impl.so
vendor/lib/hw/sensors.universal7870.so

# Soundtrigger
vendor/lib/hw/android.hardware.soundtrigger@2.0-impl.so

# Vibrator
vendor/lib/hw/vibrator.default.so

# Wi-Fi
vendor/bin/hw/android.hardware.wifi@1.0-service
vendor/bin/hw/hostapd
vendor/bin/hw/wpa_supplicant
vendor/etc/init/android.hardware.wifi@1.0-service.rc
vendor/lib/libkeystore-engine-wifi-hidl.so
vendor/lib/libkeystore-wifi-hidl.so
vendor/lib/libwifi-hal.so
vendor/lib/vendor.samsung.hardware.wifi.hostapd@2.0.so
vendor/lib/vendor.samsung.hardware.wifi.supplicant@2.0.so

# Wi-Fi configs
vendor/etc/wifi/bcmdhd_apsta.bin_36
vendor/etc/wifi/bcmdhd_clm.blob
vendor/etc/wifi/bcmdhd_mfg.bin_36
vendor/etc/wifi/bcmdhd_sta.bin_36
vendor/etc/wifi/cred.conf
vendor/etc/wifi/indoorchannel.info
vendor/etc/wifi/nvram.txt_36
vendor/etc/wifi/p2p_supplicant_overlay.conf
vendor/etc/wifi/wpa_supplicant.conf
vendor/etc/wifi/wpa_supplicant_overlay.conf

# Miscellaneous
odm/app/MyGalaxyWidget/MyGalaxyWidget.apk
odm/app/MyGalaxyWidget_NEBANGS/MyGalaxyWidget_NEBANGS.apk
odm/app/PreinstallProvider/PreinstallProvider.apk
odm/app/SamsungMax/SamsungMax.apk
odm/app/mygalaxy_Stub-derived-2.9.9.1/mygalaxy_Stub-derived-2.9.9.1.apk
odm/etc/init/init.rc
odm/etc/omc/BKD/conf/cscfeature.xml
odm/etc/omc/BKD/conf/cscfeature_network.xml
odm/etc/omc/BKD/conf/customer.xml
odm/etc/omc/BKD/conf/omc.info
odm/etc/omc/BKD/etc/BKD_keystrings.dat
odm/etc/omc/BKD/etc/byoddeletepackagenames.txt
odm/etc/omc/BKD/etc/contents.db
odm/etc/omc/BKD/etc/default_application_order.xml
odm/etc/omc/BKD/etc/default_workspace.xml
odm/etc/omc/BKD/etc/enforcedeletepackage.txt
odm/etc/omc/BKD/etc/enforceskippingpackages.txt
odm/etc/omc/BKD/etc/fonts.xml
odm/etc/omc/BKD/etc/hidden_apks_list.txt
odm/etc/omc/BKD/etc/language.xml
odm/etc/omc/BKD/etc/sales_code.dat
odm/etc/omc/BKD/opt/appres.json
odm/etc/omc/INS/conf/cscfeature.xml
odm/etc/omc/INS/conf/cscfeature_network.xml
odm/etc/omc/INS/conf/customer.xml
odm/etc/omc/INS/conf/omc.info
odm/etc/omc/INS/etc/permissions/privapp-permissions-com.aura.oobe.samsung.xml
odm/etc/omc/INS/etc/INS_keystrings.dat
odm/etc/omc/INS/etc/IN_MyGalaxy.dat
odm/etc/omc/INS/etc/byoddeletepackagenames.txt
odm/etc/omc/INS/etc/contents.db
odm/etc/omc/INS/etc/default_application_order.xml
odm/etc/omc/INS/etc/default_workspace.xml
odm/etc/omc/INS/etc/default_workspace_knox.xml
odm/etc/omc/INS/etc/enforcedeletepackage.txt
odm/etc/omc/INS/etc/enforceskippingpackages.txt
odm/etc/omc/INS/etc/hidden_apks_list.txt
odm/etc/omc/INS/etc/language.xml
odm/etc/omc/INS/etc/pre_install.appsflyer
odm/etc/omc/INS/etc/regulatory_info.png
odm/etc/omc/INS/etc/sales_code.dat
odm/etc/omc/INS/opt/appres.json
odm/etc/omc/NPB/conf/cscfeature.xml
odm/etc/omc/NPB/conf/cscfeature_network.xml
odm/etc/omc/NPB/conf/customer.xml
odm/etc/omc/NPB/conf/omc.info
odm/etc/omc/NPB/etc/NPB_keystrings.dat
odm/etc/omc/NPB/etc/byoddeletepackagenames.txt
odm/etc/omc/NPB/etc/contents.db
odm/etc/omc/NPB/etc/default_application_order.xml
odm/etc/omc/NPB/etc/default_workspace.xml
odm/etc/omc/NPB/etc/enforcedeletepackage.txt
odm/etc/omc/NPB/etc/enforceskippingpackages.txt
odm/etc/omc/NPB/etc/hidden_apks_list.txt
odm/etc/omc/NPB/etc/language.xml
odm/etc/omc/NPB/etc/sales_code.dat
odm/etc/omc/NPB/opt/appres.json
odm/etc/omc/SLI/conf/cscfeature.xml
odm/etc/omc/SLI/conf/cscfeature_network.xml
odm/etc/omc/SLI/conf/customer.xml
odm/etc/omc/SLI/conf/omc.info
odm/etc/omc/SLI/etc/SLI_keystrings.dat
odm/etc/omc/SLI/etc/byoddeletepackagenames.txt
odm/etc/omc/SLI/etc/contents.db
odm/etc/omc/SLI/etc/default_application_order.xml
odm/etc/omc/SLI/etc/default_workspace.xml
odm/etc/omc/SLI/etc/enforcedeletepackage.txt
odm/etc/omc/SLI/etc/enforceskippingpackages.txt
odm/etc/omc/SLI/etc/hidden_apks_list.txt
odm/etc/omc/SLI/etc/language.xml
odm/etc/omc/SLI/etc/sales_code.dat
odm/etc/omc/SLI/opt/appres.json
odm/etc/omc/CSCVersion.txt
odm/etc/omc/SW_Configuration.xml
odm/etc/omc/code
odm/etc/omc/omc_apks_list.txt
odm/etc/omc/omc_b2b_list
odm/etc/omc/sales_code.dat
odm/etc/omc/sales_code_list.dat
odm/priv-app/appcloud_oobe/appcloud_oobe.apk
odm/sipdb/en_gb/blacklist.bin
odm/sipdb/en_gb/capital_word_dic_cw.bin
odm/sipdb/en_gb/common_substitutions.txt
odm/sipdb/en_gb/common_transpositions.txt
odm/sipdb/en_gb/config.xml
odm/sipdb/en_gb/confusion_matrix_qwerty_distance.cm
odm/sipdb/en_gb/cvoc.txt
odm/sipdb/en_gb/dot_finished_tokens_voc.tsv
odm/sipdb/en_gb/emoji_data.bin
odm/sipdb/en_gb/emoji_list.bin
odm/sipdb/en_gb/emoji_lm.bin
odm/sipdb/en_gb/emoji_lm_helper_vocab.bin
odm/sipdb/en_gb/emoji_lm_list.bin
odm/sipdb/en_gb/emoji_lm_threshold.txt
odm/sipdb/en_gb/emoji_lm_voc.bin
odm/sipdb/en_gb/emoji_voc.bin
odm/sipdb/en_gb/en_gb.pclm
odm/sipdb/en_gb/general_ngram.bin
odm/sipdb/en_gb/general_nlm.bin
odm/sipdb/en_gb/general_nlm_filteration.bin
odm/sipdb/en_gb/general_nlm_voc.bin
odm/sipdb/en_gb/general_nlm_voc_s.bin
odm/sipdb/en_gb/hyphenwords.bin
odm/sipdb/en_gb/index_chars.txt
odm/sipdb/en_gb/language_detection.bin
odm/sipdb/en_gb/out_chars_ctc.txt
odm/sipdb/en_gb/parameters.txt
odm/sipdb/en_gb/stop_words_list.bin
odm/sipdb/en_gb/suggestion_rules.txt
odm/sipdb/en_gb/suggestion_rules_emoji.txt
odm/sipdb/en_gb/swipe_model_CTC_GRU_qwerty.bin
odm/sipdb/en_us/china/suggestion_rules.txt
odm/sipdb/en_us/common/suggestion_rules.txt
odm/sipdb/en_us/dcm/suggestion_rules.txt
odm/sipdb/en_us/jpnopen/suggestion_rules.txt
odm/sipdb/en_us/kddi/suggestion_rules.txt
odm/sipdb/en_us/korean/suggestion_rules.txt
odm/sipdb/en_us/rkt/suggestion_rules.txt
odm/sipdb/en_us/blacklist.bin
odm/sipdb/en_us/capital_word_dic_cw.bin
odm/sipdb/en_us/common_substitutions.txt
odm/sipdb/en_us/common_transpositions.txt
odm/sipdb/en_us/config.xml
odm/sipdb/en_us/confusion_matrix_qwerty_distance.cm
odm/sipdb/en_us/confusion_matrix_typos.tsv
odm/sipdb/en_us/cvoc.txt
odm/sipdb/en_us/dot_finished_tokens_voc.tsv
odm/sipdb/en_us/emoji_data.bin
odm/sipdb/en_us/emoji_list.bin
odm/sipdb/en_us/emoji_lm.bin
odm/sipdb/en_us/emoji_lm_helper_vocab.bin
odm/sipdb/en_us/emoji_lm_list.bin
odm/sipdb/en_us/emoji_lm_threshold.txt
odm/sipdb/en_us/emoji_lm_voc.bin
odm/sipdb/en_us/emoji_voc.bin
odm/sipdb/en_us/en_us.pclm
odm/sipdb/en_us/general_ngram.bin
odm/sipdb/en_us/general_nlm.bin
odm/sipdb/en_us/general_nlm_filteration.bin
odm/sipdb/en_us/general_nlm_voc.bin
odm/sipdb/en_us/general_nlm_voc_s.bin
odm/sipdb/en_us/hyphenwords.bin
odm/sipdb/en_us/index_chars.txt
odm/sipdb/en_us/language_detection.bin
odm/sipdb/en_us/out_chars_ctc.txt
odm/sipdb/en_us/parameters.txt
odm/sipdb/en_us/stop_words_list.bin
odm/sipdb/en_us/suggestion_rules.txt
odm/sipdb/en_us/suggestion_rules_emoji.txt
odm/sipdb/en_us/swipe_model_CTC_GRU_qwerty.bin
odm/sipdb/Samsung_1102_r1-2_ASlsUN_xt9_ALM3.ldb
odm/sipdb/Samsung_1102_r1-2_BNlsUN_xt9_ALM3.ldb
odm/sipdb/Samsung_1102_r1-2_KNlsUN_xt9_ALM3.ldb
odm/sipdb/Samsung_1102_r1-2_NElsUN_xt9_ALM3.ldb
odm/sipdb/Samsung_1102_r1-2_ORlsUN_xt9_ALM3.ldb
odm/sipdb/Samsung_1102_r1-2_PAlsUN_xt9_ALM3.ldb
odm/sipdb/Samsung_1102_r1-2_SIlsUNAlternate_xt9_ALM3.ldb
odm/sipdb/Samsung_1102_r1-5_GUlsUN_xt9_ALM3.ldb
odm/sipdb/Samsung_1102_r1-5_MRlsUN_xt9_ALM3.ldb
odm/sipdb/Samsung_1102_r1-5_URlsUN_xt9_ALM3.ldb
odm/sipdb/Samsung_1102_r1-6_HIlsUN_xt9_ALM3.ldb
odm/sipdb/Samsung_1102_r1-6_MLlsUN_xt9_ALM3.ldb
odm/sipdb/Samsung_1301_r1-6_TElsUN_xt9_ALM3.ldb
odm/sipdb/Samsung_1301_r1-7_TAlsUN_xt9_ALM3.ldb
vendor/app/mcRegistry/00060308060501020000000000000000.tlbin
vendor/app/mcRegistry/07010000000000000000000000000000.tlbin
vendor/app/mcRegistry/07060000000000000000000000000000.tlbin
vendor/app/mcRegistry/08130000000000000000000000000000.tlbin
vendor/app/mcRegistry/FFFFFFFF000000000000000000000001.drbin
vendor/app/mcRegistry/ffffffff000000000000000000000005.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000000b.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000000c.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000000d.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000017.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000002f.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000003e.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000045.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000059.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000060.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000077.tlbin
vendor/app/mcRegistry/ffffffffd0000000000000000000000a.tlbin
vendor/app/mcRegistry/ffffffffd00000000000000000000016.tlbin
vendor/bin/hw/gpsd
vendor/bin/hw/macloader
vendor/bin/hw/mfgloader
vendor/bin/hw/rild
vendor/bin/hw/vendor.samsung.hardware.base@1.0-service
vendor/bin/hw/vendor.samsung.hardware.camera.provider@3.0-service
vendor/bin/hw/vendor.samsung.hardware.gnss@2.0-service
vendor/bin/hw/vendor.samsung.hardware.light@3.0-service
vendor/bin/hw/vendor.samsung.hardware.miscpower@2.0-service
vendor/bin/hw/vendor.samsung.hardware.snap@1.1-service
vendor/bin/hw/vendor.samsung.hardware.vibrator@2.0-service
vendor/bin/hw/vendor.samsung.hardware.wifi@2.0-service
vendor/bin/hw/vendor.samsung_slsi.hardware.ExynosHWCServiceTW@1.0-service
vendor/bin/hw/vendor.samsung_slsi.hardware.configstore@1.0-service
vendor/bin/bc
vendor/bin/cbd
vendor/bin/devmem
vendor/bin/dumpsys
vendor/bin/fsync
vendor/bin/getconf
vendor/bin/i2cdetect
vendor/bin/i2cdump
vendor/bin/i2cget
vendor/bin/i2cset
vendor/bin/iconv
vendor/bin/install
vendor/bin/mcDriverDaemon
vendor/bin/nc
vendor/bin/netcat
vendor/bin/nproc
vendor/bin/nsenter
vendor/bin/secril_config_svc
vendor/bin/snap_utility_32
vendor/bin/snap_utility_64
vendor/bin/test_model.json
vendor/bin/unlink
vendor/bin/unshare
vendor/bin/uuidgen
vendor/bin/vaultkeeperd
vendor/bin/vendor.samsung.hardware.security.vaultkeeper@1.0-service
vendor/bin/vendor.samsung.hardware.security.widevine.keyprovisioning@1.0-service
vendor/bin/watch
vendor/bin/wvkprov
vendor/etc/carrier/ACG/permissions/android.hardware.telephony.cdma.xml
vendor/etc/carrier/BST/permissions/android.hardware.telephony.cdma.xml
vendor/etc/carrier/CCT/permissions/android.hardware.telephony.cdma.xml
vendor/etc/carrier/LRA/permissions/android.hardware.telephony.cdma.xml
vendor/etc/carrier/SPR/permissions/android.hardware.telephony.cdma.xml
vendor/etc/carrier/TFN/permissions/android.hardware.telephony.cdma.xml
vendor/etc/carrier/USC/permissions/android.hardware.telephony.cdma.xml
vendor/etc/carrier/VMU/permissions/android.hardware.telephony.cdma.xml
vendor/etc/carrier/VZW/permissions/android.hardware.telephony.cdma.xml
vendor/etc/carrier/XAS/permissions/android.hardware.telephony.cdma.xml
vendor/etc/gnss/ca.pem
vendor/etc/gnss/gps.cfg
vendor/etc/init/hostapd.android.rc
vendor/etc/init/init.baseband.rc
vendor/etc/init/init.gps.rc
vendor/etc/init/init.samsung.bsp.rc
vendor/etc/init/init.samsungexynos7870.rc
vendor/etc/init/init.samsungexynos7870.usb.rc
vendor/etc/init/init.vendor.onebinary.rc
vendor/etc/init/init.vendor.rilchip.rc
vendor/etc/init/init.vendor.rilcommon.rc
vendor/etc/init/mobicore.rc
vendor/etc/init/snap_utility.rc
vendor/etc/init/vaultkeeper_common.rc
vendor/etc/init/vendor.samsung.hardware.base@1.0-service.rc
vendor/etc/init/vendor.samsung.hardware.camera.provider@3.0-service.rc
vendor/etc/init/vendor.samsung.hardware.gnss@2.0-service.rc
vendor/etc/init/vendor.samsung.hardware.light@3.0-service.rc
vendor/etc/init/vendor.samsung.hardware.miscpower@2.0-service.rc
vendor/etc/init/vendor.samsung.hardware.security.widevine.keyprovisioning@1.0-service.rc
vendor/etc/init/vendor.samsung.hardware.snap@1.1-service.rc
vendor/etc/init/vendor.samsung.hardware.vibrator@2.0-service.rc
vendor/etc/init/vendor.samsung.hardware.wifi@2.0-service.rc
vendor/etc/init/vendor.samsung_slsi.hardware.ExynosHWCServiceTW@1.0-service.rc
vendor/etc/init/vendor.samsung_slsi.hardware.configstore@1.0-service.rc
vendor/etc/init/vndservicemanager.rc
vendor/etc/init/wifi.rc
vendor/etc/init/wifi_brcm.rc
vendor/etc/nxp/speechassist/BargeIn/Tx_ControlParams_WIDEBAND_ANALOG_DOCK.txt
vendor/etc/nxp/speechassist/BargeIn/Tx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/speechassist/BargeIn/Tx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/speechassist/BargeIn/Tx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/speechassist/BargeIn/Tx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/speechassist/BargeIn/Tx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/speechassist/BargeInDriving/Tx_ControlParams_WIDEBAND_ANALOG_DOCK.txt
vendor/etc/nxp/speechassist/BargeInDriving/Tx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/speechassist/BargeInDriving/Tx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/speechassist/BargeInDriving/Tx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/speechassist/BargeInDriving/Tx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/speechassist/BargeInDriving/Tx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/speechassist/default/Rx_ControlParams_EARPIECE_16000Hz.txt
vendor/etc/nxp/speechassist/default/Rx_ControlParams_SPEAKER_16000Hz.txt
vendor/etc/nxp/speechassist/default/Tx_ControlParams_EARPIECE_16000Hz.txt
vendor/etc/nxp/speechassist/default/Tx_ControlParams_SPEAKER_16000Hz.txt
vendor/etc/nxp/speechassist/LVVEFS_Rx_Configuration.txt
vendor/etc/nxp/speechassist/LVVEFS_Tx_Configuration.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Rx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Rx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET_NREC_ON.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Rx_ControlParams_NARROWBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Rx_ControlParams_NARROWBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Rx_ControlParams_NARROWBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Rx_ControlParams_NARROWBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Rx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Rx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET_NREC_ON.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Rx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Rx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Rx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Rx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Tx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Tx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET_NREC_ON.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Tx_ControlParams_NARROWBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Tx_ControlParams_NARROWBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Tx_ControlParams_NARROWBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Tx_ControlParams_NARROWBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Tx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Tx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET_NREC_ON.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Tx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Tx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Tx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/VideoTelephony/Tx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Rx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Rx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET_NREC_ON.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Rx_ControlParams_NARROWBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Rx_ControlParams_NARROWBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Rx_ControlParams_NARROWBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Rx_ControlParams_NARROWBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Rx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Rx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET_NREC_ON.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Rx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Rx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Rx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Rx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Tx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Tx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET_NREC_ON.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Tx_ControlParams_NARROWBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Tx_ControlParams_NARROWBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Tx_ControlParams_NARROWBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Tx_ControlParams_NARROWBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Tx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Tx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET_NREC_ON.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Tx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Tx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Tx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/WifiCalling/Tx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Rx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Rx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET_NREC_ON.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Rx_ControlParams_NARROWBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Rx_ControlParams_NARROWBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Rx_ControlParams_NARROWBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Rx_ControlParams_NARROWBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Rx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Rx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET_NREC_ON.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Rx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Rx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Rx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Rx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Tx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Tx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET_NREC_ON.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Tx_ControlParams_NARROWBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Tx_ControlParams_NARROWBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Tx_ControlParams_NARROWBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Tx_ControlParams_NARROWBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Tx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Tx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET_NREC_ON.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Tx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Tx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Tx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/WifiCallingHAC/Tx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIP/Rx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIP/Rx_ControlParams_NARROWBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/mVoIP/Rx_ControlParams_NARROWBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/mVoIP/Rx_ControlParams_NARROWBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/mVoIP/Rx_ControlParams_NARROWBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIP/Rx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIP/Rx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/mVoIP/Rx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/mVoIP/Rx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/mVoIP/Rx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIP/Tx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIP/Tx_ControlParams_NARROWBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/mVoIP/Tx_ControlParams_NARROWBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/mVoIP/Tx_ControlParams_NARROWBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/mVoIP/Tx_ControlParams_NARROWBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIP/Tx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIP/Tx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/mVoIP/Tx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/mVoIP/Tx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/mVoIP/Tx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPFMC/Rx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPFMC/Rx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/mVoIPFMC/Rx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/mVoIPFMC/Rx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/mVoIPFMC/Rx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPFMC/Tx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPFMC/Tx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/mVoIPFMC/Tx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/mVoIPFMC/Tx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/mVoIPFMC/Tx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Rx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Rx_ControlParams_NARROWBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Rx_ControlParams_NARROWBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Rx_ControlParams_NARROWBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Rx_ControlParams_NARROWBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Rx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Rx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Rx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Rx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Rx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Tx_ControlParams_NARROWBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Tx_ControlParams_NARROWBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Tx_ControlParams_NARROWBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Tx_ControlParams_NARROWBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Tx_ControlParams_NARROWBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Tx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Tx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Tx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Tx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/mVoIPHAC/Tx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPSec/Rx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPSec/Rx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/mVoIPSec/Rx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/mVoIPSec/Rx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/mVoIPSec/Rx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPSec/Tx_ControlParams_WIDEBAND_BLUETOOTH_SCO_HEADSET.txt
vendor/etc/nxp/voiceexperience/mVoIPSec/Tx_ControlParams_WIDEBAND_EARPIECE.txt
vendor/etc/nxp/voiceexperience/mVoIPSec/Tx_ControlParams_WIDEBAND_SPEAKER.txt
vendor/etc/nxp/voiceexperience/mVoIPSec/Tx_ControlParams_WIDEBAND_WIRED_HEADPHONE.txt
vendor/etc/nxp/voiceexperience/mVoIPSec/Tx_ControlParams_WIDEBAND_WIRED_HEADSET.txt
vendor/etc/nxp/voiceexperience/LVVEFS_Rx_Configuration.txt
vendor/etc/nxp/voiceexperience/LVVEFS_Tx_Configuration.txt
vendor/etc/nxp/AzControlParams_SPEAKER.txt
vendor/etc/nxp/LVAZFS_Configuration.txt
vendor/etc/saiv/fd/fast_face_detect_model_full_angle_16_CHAR.dat
vendor/etc/saiv/gae/MeanFea.bin
vendor/etc/saiv/gae/ParaFea.bin
vendor/etc/saiv/gae/PmAgeFeatSelIdx.bin
vendor/etc/saiv/gae/PmAgeModel.bin
vendor/etc/saiv/gae/PmExpFeatSelIdx.bin
vendor/etc/saiv/gae/PmExpModel.bin
vendor/etc/saiv/gae/PmGenModel.bin
vendor/etc/saiv/gae/PmGenderFeatSelIdx.bin
vendor/etc/saiv/ld/avg_hog_13pt.dat
vendor/etc/saiv/ld/avg_hog_35pt.dat
vendor/etc/saiv/ld/mean_35pt.txt
vendor/etc/saiv/ld/regress_matrix_short_13pt.dat
vendor/etc/saiv/ld/regress_matrix_short_35pt.dat
vendor/etc/seccomp_policy/mediaextractor_sec.policy
vendor/etc/str/str_preference_data.dat
vendor/etc/vintf/manifest/vendor.samsung.hardware.base@1.0-service.xml
vendor/etc/SoundBoosterParam.txt
vendor/etc/Tfa9896.cnt
vendor/etc/floating_feature.xml
vendor/etc/fstab.samsungexynos7870
vendor/etc/mixer_gains.xml
vendor/etc/mkshrc
vendor/etc/mtu-conf.xml
vendor/etc/pdpcnt-conf.xml
vendor/etc/plmn_delta.bin
vendor/etc/plmn_delta_attaio.bin
vendor/etc/plmn_delta_hktw.bin
vendor/etc/plmn_delta_usacdma.bin
vendor/etc/plmn_delta_usagsm.bin
vendor/etc/plmn_se13.bin
vendor/etc/public.libraries.txt
vendor/etc/public.libraries.txt.backup
vendor/etc/snap_gpu_kernel_32.bin
vendor/etc/snap_gpu_kernel_64.bin
vendor/etc/somxreg.conf
vendor/etc/wlan_common_rc
vendor/etc/wlan_vendor_rc
vendor/firmware/bcm43436B0.hcd
vendor/firmware/fimc_is_lib.bin
vendor/firmware/mfc_fw.bin
vendor/firmware/setfile_3l2.bin
vendor/firmware/setfile_5e9.bin
vendor/firmware/setfile_5e9_front.bin
vendor/lib/hw/audio.sec_primary.default.so
vendor/lib/hw/vendor.samsung.hardware.audio@1.0-impl.so
vendor/lib/hw/vendor.samsung.hardware.camera.provider@3.0-impl.so
vendor/lib/hw/vendor.samsung.hardware.gnss@2.0-impl.so
vendor/lib/hw/vendor.samsung.hardware.light@3.0-impl.so
vendor/lib/hw/vendor.samsung.hardware.snap@1.1-impl.so
vendor/lib/omx/libOMX.Exynos.AVC.Decoder.so
vendor/lib/omx/libOMX.Exynos.AVC.Encoder.so
vendor/lib/omx/libOMX.Exynos.HEVC.Decoder.so
vendor/lib/omx/libOMX.Exynos.HEVC.Encoder.so
vendor/lib/omx/libOMX.Exynos.MPEG4.Decoder.so
vendor/lib/omx/libOMX.Exynos.MPEG4.Encoder.so
vendor/lib/omx/libOMX.Exynos.VP8.Decoder.so
vendor/lib/omx/libOMX.Exynos.VP8.Encoder.so
vendor/lib/omx/libOMX.Exynos.WMV.Decoder.so
vendor/lib/libExynosOMX_Core.so
vendor/lib/libExynosOMX_Resourcemanager.so
vendor/lib/libFrucSSMLib.so
vendor/lib/libMcRegistry.so
vendor/lib/libOpenCL.so
vendor/lib/libOpenCL.so.1
vendor/lib/libOpenCL.so.1.1
vendor/lib/libOpenCv.camera.samsung.so
vendor/lib/libapex_cmn.so
vendor/lib/libapex_utils.so
vendor/lib/libbt-vendor.so
vendor/lib/libdsms_vendor.so
vendor/lib/libpredeflicker_native.so
vendor/lib/libsavscmn.so
vendor/lib/libsec-ril-dsds.so
vendor/lib/libsec-ril.so
vendor/lib/libsnap_caffe.so
vendor/lib/libsnap_caffe_wrapper.so
vendor/lib/libsnap_compute.so
vendor/lib/libsnap_compute_wrapper.so
vendor/lib/libsnap_v1.samsung.so
vendor/lib/libsnap_vndk.so
vendor/lib/libsnaplite_native.so
vendor/lib/libsnaplite_wrapper.so
vendor/lib/libsomxcore_vendor.so
vendor/lib/libswldc_capture_core.camera.samsung.so
vendor/lib/libvkmanager_vendor.so
vendor/lib/libvkservice.so
vendor/lib/libwpa_client.so
vendor/lib/libwrappergps.so
vendor/lib/libxcv.camera.samsung.so
vendor/lib/vendor.samsung.camera.device@1.0-impl.so
vendor/lib/vendor.samsung.camera.device@4.0-impl.so
vendor/lib/vendor.samsung.frameworks.security.dsms@1.0.so
vendor/lib/vendor.samsung.frameworks.security.ucm.crypto@1.0.so
vendor/lib/vendor.samsung.hardware.base@1.0.so
vendor/lib/vendor.samsung.hardware.biometrics.face@1.0.so
vendor/lib/vendor.samsung.hardware.bluetooth.a2dp@1.0.so
vendor/lib/vendor.samsung.hardware.camera.device@1.0.so
vendor/lib/vendor.samsung.hardware.camera.device@4.0.so
vendor/lib/vendor.samsung.hardware.camera.provider@3.0.so
vendor/lib/vendor.samsung.hardware.gnss@2.0.so
vendor/lib/vendor.samsung.hardware.light@3.0.so
vendor/lib/vendor.samsung.hardware.miscpower@2.0.so
vendor/lib/vendor.samsung.hardware.radio.bridge@2.0.so
vendor/lib/vendor.samsung.hardware.radio.bridge@2.0_vendor.so
vendor/lib/vendor.samsung.hardware.radio.channel@2.0_vendor.so
vendor/lib/vendor.samsung.hardware.radio@2.0_vendor.so
vendor/lib/vendor.samsung.hardware.security.vaultkeeper@1.0.so
vendor/lib/vendor.samsung.hardware.security.widevine.keyprovisioning@1.0.so
vendor/lib/vendor.samsung.hardware.snap@1.0.so
vendor/lib/vendor.samsung.hardware.vibrator@2.0.so
vendor/lib/vendor.samsung.hardware.wifi@2.0.so
vendor/lib/vendor.samsung_slsi.hardware.ExynosHWCServiceTW@1.0.so
vendor/lib/vendor.samsung_slsi.hardware.configstore-utils.so
vendor/lib/vendor.samsung_slsi.hardware.configstore@1.0.so
vendor/priv-app/wallpaper-res/wallpaper-res.apk
vendor/manifest.xml
